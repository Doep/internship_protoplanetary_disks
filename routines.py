
import numpy as np
from astropy.io import fits
import os
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib as mpl
from scipy.interpolate import interp1d
from scipy.ndimage import rotate
from matplotlib.ticker import MultipleLocator

cazzoletti_directory = '/user/homedir/demarsd/Documents/stage/modeles/cazzoletti_extended_datas/'
sls_file = '/user/homedir/demarsd/Documents/lut/sls.lut'

uma_si = 1.6605e-27     # uma value in Kg
uma_cgs = 1.6605e-24    # uma value in g
au_si = 1.496e11        # au value in m
au_cgs = 1.496e13       # au value in cm
M_sun_si = 1.9884e30    # M_sun in Kg
M_sun_cgs = 1.9884e33   # M_sun in g
c_si = 299792458           # c in m/s

#_________________________________________________________________________________
#_________________________________________________________________________________
# Turns a fits file into a list of its sub arrays
def fits_to_array(filename):
    file = fits.open(filename)
    array = []
    for i in range (len(file)):
        array.append(file[i].data)
    file.close()
    return array

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING Basically useless
# Lines are counted as starting at 0, won't produce a file if a line's data has more than 2 axis
def fits_to_txtfile(filename):
    array = fits_to_array(filename)
    with fits.open(filename) as file:
        for i in range(len(array)):
            if file[i].header['NAXIS'] <=2:
                np.savetxt(os.path.splitext(filename)[0] + '_data_line_{_of_{}.txt'.format(i,len(array)-1), array[i], delimiter=',')

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING This function is obsolete, use grid_parameters instead and check r[i],z[i] values instead
# Gives parameters of the cell for a given (line, n_r, n_z, n_az) cell. Output : (radius, height)
# WARNING : (line, n_r, n_z, n_az) all start at 0 : 1st line is line=0
def param_cell(line, n_r, n_z, n_az, array):
    r = array[line][0][n_az][n_z][n_r]
    z = array[line][1][n_az][n_z][n_r]
    return [r,z]


#_________________________________________________________________________________
#_________________________________________________________________________________
# Reads grid parameters
# Returns 2D arrays of R and z parameters
def grid_parameters(filename):
    grid_array = fits_to_array(filename)
    r = np.transpose(grid_array[0][0][0])
    z = np.transpose(grid_array[0][1][0])
    return [r,z]


#_________________________________________________________________________________
#_________________________________________________________________________________
# Turns an array into a fits file. For convenience, use a numpy array type.
def array_to_fits(name, array):
    hdu = fits.PrimaryHDU(array)
    hdul = fits.HDUList([hdu])
    hdul.writeto(name,overwrite=True)

#_________________________________________________________________________________
#_________________________________________________________________________________
# Replaces commas with dots in given file
def comma_to_dot(file):
    with open(file, 'r') as f:
        filedata = f.read()
    filedata = filedata.replace(',', '.')
    with open(file, 'w') as f:
        f.write(filedata)

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING Basically useless
# Turns file into an array, also changes z/R into z
def file_to_array(file):
    array = np.loadtxt(file, delimiter = ";")
    for i in range (len(array)):
        array[i][1] = array[i][0] * array[i][1]
    return array

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING not really useful anymore, this is too specific
# Reads the chemical structure
# Turns z/R into z
# Applies a resizing factor
# Applies a z resizing factor
def read_chem_structure(file,expand_z,resize_factor):
    rchem,zchem = np.loadtxt(file, delimiter = ";", unpack=True, usecols=[0,1])
    resize = resize_factor
    for i in range(len(rchem)):
        zchem[i] = zchem[i] * rchem[i] * expand_z * resize
        rchem[i] = rchem[i] * resize
    return [rchem,zchem]

#_________________________________________________________________________________
#_________________________________________________________________________________
# Copy header from a fits file to another 
def copy_header(src_nm, dst_nm, ignore_blank = False, ignore_zero_scaling = True):
    src = fits.open(src_nm)
    dst = fits.open(dst_nm, 'update')
    hdr_src = src[0].header
    hdr_dst = dst[0].header
    for i in hdr_src:
        try:
            if str(i) == 'BLANK' and ignore_blank == True:
                print('ignoring BLANK card')
            elif (str(i) == 'BSCALE' or str(i) == 'BZERO') and ignore_zero_scaling == True:
                print('ignoring BSCALE / BZERO card')
            elif str(i) == 'DATAMIN':
                hdr_dst[str(i)] = np.min(fits_to_array(dst_nm)[0])
                hdr_dst.comments[str(i)] = hdr_src.comments[str(i)]
            elif str(i) == 'DATAMAX':
                hdr_dst[str(i)] = np.max(fits_to_array(dst_nm)[0])
                hdr_dst.comments[str(i)] = hdr_src.comments[str(i)]
            else:
                hdr_dst[str(i)] = hdr_src[str(i)]
                hdr_dst.comments[str(i)] = hdr_src.comments[str(i)]
        except:
            print('header or comment line : ' + i + ' could not be copied')
    src.close()
    dst.close()

#_________________________________________________________________________________
#_________________________________________________________________________________
# Rotates a cube, angle is defined positive in the trigonometric way
def rotate_cube(filename, angle):
    angle = - angle
    ar = fits_to_array(filename)[0]
    rot = rotate(ar, angle = angle, axes = (1,2), reshape = False)
    savename = os.path.splitext(filename)[0] + '_rotated.fits'
    array_to_fits(savename, rot)
    copy_header(filename, savename, ignore_zero_scaling = True)

#_________________________________________________________________________________
#_________________________________________________________________________________
# Plots the sign of the difference between two fits files
def fits_diff(filename, savename = 'diff.fits'):
    array = fits_to_array(filename)[0]
    array[array > 0] = 1
    array[array < 0] = -1
    array_to_fits(savename, array)



# Make a list of coordinates (in arcsec) from a given cube
# Also makes a cube containing said coordinates if make_cube is set to True
def coordinates(filename, make_cube = False, savename = 'coordinates.fits'):
    xlist = make_coord_list(filename, axis = 1, no_crval = True)#, abs_cdelt = True)
    ylist = make_coord_list(filename, axis = 2, no_crval = True)#, abs_cdelt = True)
    # ARCSEC coordinates array. pos[0] = x, pos[1] = y
    if make_cube == True:
        ar = fits_to_array(filename)[0]
        pos = np.zeros(np.shape(ar[0]))
        pos = [pos,pos]
        for i in range(xsize):
            for j in range(ysize):
                pos[0][j][i] = xlist[i]
                pos[1][j][i] = ylist[j]
        array_to_fits(savename, pos)
    xlist = np.dot(xlist, 3600)
    ylist = np.dot(ylist, 3600)
    return [xlist,ylist]
    
    

#_________________________________________________________________________________
#_________________________________________________________________________________
# Make the list of value along the given axis
def make_coord_list(filename,
                    axis,
                    no_crval = False,
                    abs_cdelt = False,):
    lines = fits.open(filename)
    hdr = lines[0].header
    axlength = hdr['NAXIS' + str(axis)]
    if no_crval == False:
        crval = hdr['CRVAL' + str(axis)]
    else:
        crval = 0
    cdelt = hdr['CDELT' + str(axis)]
    if abs_cdelt == True:
        cdelt = abs(cdelt)
    crpix = hdr['CRPIX' + str(axis)]
    coord_list = []
    for i in range(axlength):
        pix_val = crval + (i-(crpix-1))*cdelt
        coord_list.append(pix_val)
    return coord_list


#_________________________________________________________________________________
#_________________________________________________________________________________
# Sum all three rays
def sum_rays(savename = 'sum_hyperfine.fits',
             nu_list = None,
             v_lsr = 0,
             prefix = 'ray'):
    
    if nu_list == None:
        i = 0
        nu_list = []
        while os.path.isfile(prefix + str(i) + '.fits') == True:
            ray_name = prefix + str(i) + '.fits'
            ray_file = fits.open(ray_name)
            nu_ref = ray_file[0].header['NU_REF']
            nu_list.append(nu_ref)
            ray_file.close()
            i = i + 1
    
    # Removing Nones and NaNs
    for k in range(len(nu_list)):
        if nu_list[k] is None:
            nu_list[k] = np.nan
    test = np.isnan(nu_list)
    ind = []
    for k in range(len(test)):
        if test[k] == True:
            ind.append(k)
    nu_list = np.delete(nu_list, ind)
    
    # Taking nu0 and removing it
    nu0 = nu_list[0]
    nu_list = np.delete(nu_list,0)
    ray_n = len(nu_list)
    
    #Fits to arrays
    rays = []
    for k in range(ray_n +1):
        rays.append(fits_to_array(prefix + str(k) + '.fits')[0])
        
    #ray0 = fits_to_array(file_ray0)[0]
    #ray1 = fits_to_array(file_ray1)[0]
    #ray2 = fits_to_array(file_ray2)[0]
    
    c = c_si /1000  # c in km/s
    
    file_ray0 = prefix + '0.fits'
    fits_ray0 = fits.open(file_ray0)
    slices = fits_ray0[0].header['NAXIS3']       # Number of v slices
    v_interv = fits_ray0[0].header['CDELT3']     # Speed interval between slices
    
    # Entire list of velocities
    slice_min = -int(slices/2)
    slice_max = int(slices/2)
    v = make_coord_list(file_ray0, axis = 3)
    #print(v)
    #exit(0)
    
    # Applying Doppler
    v_list = []
    for k in range(ray_n):
        delta_v = -c * (nu_list[k] - nu0)/nu0
        v_list.append(np.subtract(v, delta_v))
        #print(v_list[k])
        #v_list.append([c*(nu_list[k]/nu0 -1) + i*nu_list[k]/nu0 for i in v])
    #v1 = [c*(nu1/nu0 -1) + i*nu1/nu0 for i in v]
    #v2 = [c*(nu2/nu0 -1) + i*nu2/nu0 for i in v]
    #exit(0)
    
    v_list = np.array(v_list)
    
    v_min = np.min(v)
    v_max = np.max(v)
    print('vmin,vmax', v_min,v_max)
    # Remove slices out of range
    for k in range(ray_n):
        for i in range(slices):
            if v_list[k][i]<v_min or v_list[k][i]>v_max:
                v_list[k][i] = np.nan
            #else:
                #v_list[k][i] -= v_min
        #print(v_list[k])
    #exit(0)

    
    # Get the slices indices
    isnan = []
    indices = []
    for k in range(ray_n):
        indi_k = []
        isnan.append(np.isnan(v_list[k]))
        for i in range(slices):
            if isnan[k][i] == False:
                indi_k.append(find_nearest(v,v_list[k][i]))
                #v_list[k][i] = int(np.round(v_list[k][i]/v_interv))
            else:
                indi_k.append(np.nan)
        indices.append(indi_k)
    
    # Print corresponding indexes
    for k in range(ray_n):
        print('list v' +str(k +1) + ' :')
        print(indices[k])
    
    # Checks that there are no duplicates slices
    print('_______________________________________________________')
    print('_______________________________________________________')
    list_indices = []
    unique = []
    for k in range(ray_n):
        list_indices.append([indices[k][i] for i in range(slices) if isnan[k][i] == False])
        list_unique, occurences = np.unique(list_indices[k], return_counts = True)
        a = [list_unique, occurences]
        unique.append(a)
        #list_v1 = [i for i in v1 if i != None]
        #list_v2 = [i for i in v2 if i != None]
        #list1, num_v1 = np.unique(list_v1, return_counts = True)
        #list2, num_v2 = np.unique(list_v2, return_counts = True)
    
    for k in range(ray_n):
        print('Checking list v' + str(k+1) + ' for duplicates')
        check = False
        for i in range(len(unique[k][1])):
            if unique[k][1][i] != 1:
                print('index ' + str(unique[k][0][i]) + ' appears ' + str(unique[k][1][i]) + ' times')
                check = True
        if check == False:
            print('No duplicates')
    
    #print('Checking list v2 for duplicates')
    #check = False
    #for i in range(len(num_v2)):
        #if num_v2[i] != 1:
            #print(str(list2[i]) + ' appears ' + str(num_v2[i]) + ' times')
            #check = True
    #if check == False:
        #print('No duplicates')
    print('_______________________________________________________')
    print('_______________________________________________________')
    
    # Summing rays
    array = np.zeros(np.shape(rays[0]))
    for i in range(slices):
        val = np.zeros(np.shape(rays[0][0]))
        for k in range(ray_n +1):
            if k ==0:
                val = np.add(val,rays[k][i])
            else:
                c = k-1
                #print(indices[c][i])
                if isnan[c][i] == False:
                    val = np.add(val, rays[k][indices[c][i]])
        array[i] = val
        #if v1[i] != None:
            #val = np.add(val,ray1[v1[i]])
        #if v2[i] != None:
            #val = np.add(val,ray2[v2[i]])

    
    array_to_fits(savename, array)
    copy_header(file_ray0,savename)
    file_final = fits.open(savename, 'update')
    hdr = file_final[0].header
    hdr['NU_REF'] = nu0
    file_final.close()

#_________________________________________________________________________________
#_________________________________________________________________________________
# Removes continuum from the lines.fits file
def continuum_removal(filename = 'lines.fits', output_prefix = 'ray'):
    # Extracting arrays
    lines = fits.open(filename)
    line = fits_to_array(filename)
    rays = line[0]
    continuum = line[1]
    
    # Removing length-1 axis'
    rays = rays[0][0]
    continuum = continuum[0][0]
    
    # Axis parameters
    NAXIS3 = lines[0].header['NAXIS3']
    NAXIS4 = lines[0].header['NAXIS4']
    # Removing continuum
    for i in range(NAXIS4):
        print('step : ' + str(i) + '/'+str(NAXIS4 -1))
        channel_ray       = rays[i]
        channel_continuum = continuum[i]
        temp_ray = []
        for k in range(NAXIS3):
            #print('step : ' + str(i) + '/'+str(NAXIS4 -1) + ', ' +str(k) + '/' +str(NAXIS3 -1))
            val = channel_ray[k] - channel_continuum
            temp_ray.append(val)
        savename = output_prefix + str(i) + '.fits'
        savename_diff = 'diff_' + savename
        array_to_fits(savename, temp_ray)
        fits_diff(savename, savename = savename_diff)
        # Adding headers
        copy_header(filename, savename)
        copy_header(savename, savename_diff)
        # Adding reference frequency header
        nu_ref = line[3][i]
        ray_file = fits.open(savename, 'update')
        hdr = ray_file[0].header
        hdr['NU_REF'] = nu_ref
        ray_diff_file = fits.open(savename_diff, 'update')
        hdr_diff = ray_diff_file[0].header
        hdr_diff['NU_REF'] = nu_ref
        ray_file.close()
        ray_diff_file.close()
        

#_________________________________________________________________________________
#_________________________________________________________________________________
# Reads a dits file and plot it
def pl_fits(filename,
            savename,
            colormin = None,
            colormax = None,
            colormap = 'gist_rainbow_r',
            xscale = 'log',
            xmin = None,
            xmax = None,
            ymin = None,
            ymax = None,
            title = None,
            draw_contours = False,
            manual_labels = False,
            plot_z = False,
            grid_file = 'grid.fits',
            font = 15,
            c_interv = 10,
            map_scale = None,
            savefig = True,
            show = False,
            close = True):
    #
    r,z = grid_parameters(grid_file)
    data = fits_to_array(filename)[0]
    data = np.transpose(data)
    
    if xmin is None:
        xmin = 1
    if xmax is None:
        xmax = np.max(r)
    
    if ymin is None:
        ymin = 0
    if ymax is None:
        if plot_z == False:
            ymax = np.max(z/r)
        else:
            ymax = np.max(z)
    
    if plot_z == False:
        y_z = z/r
    else:
        y_z = z
    
    if colormax is None:
        colormax = np.max(data)
    if colormin is None:
        colormin = colormax / 1e9
    
    mpl.rcParams['font.size'] = font
    if np.sign(colormin) == np.sign(colormax) or map_scale == 'log':
        norm = colors.LogNorm()
        lbl = 1
    if np.sign(colormin) != np.sign(colormax) or map_scale == 'linear':
        norm = colors.Normalize()
        lbl = 2
    
    plt.pcolor(r, y_z, data, vmin = colormin, vmax = colormax, norm = norm, cmap = colormap)
    plt.colorbar()
    if title != None:
        plt.title(title)
    if xscale != None:
        plt.xscale(xscale)
    plt.xlim((xmin,xmax))
    plt.ylim((ymin,ymax))
    plt.xlabel('R (au)')
    if plot_z == False:
        plt.ylabel('z/R')
    else:
        plt.ylabel('z')
    if draw_contours == True:
        if lbl == 1:
            contours = np.arange(int(np.log10(colormin)),int(np.log10(colormax))+1)
            contours = np.power(float(10), contours)
            fmt = '%.1e'
        if lbl == 2:
            contours = np.arange(colormin,colormax, c_interv)
            fmt = '%d'
        cs = plt.contour(r, y_z, data, contours, colors = 'black')
        cs.clabel(contours, inline = 1, fmt = fmt, colors = 'black', manual = manual_labels)
    if show == True:
        plt.show()
    if savefig == True:
        plt.savefig(savename, bbox_inches = 'tight')
    if close == True:
        plt.close()



#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING Obsolete, use pl_fits instead
# Reads abundance fits file and plot it on a graph
def pl_abundance(filename = 'abundance.fits', savename = 'abundance_graph.png', colormin = 1e-13, colormax = 1e-06, colormap = 'gist_rainbow_r', xscale = 'log', xmin = None, xmax = None, ymin = None, ymax = None, draw_contours = False, manual_labels = False, grid_file = 'grid.fits', plot_z = False):
    r,z = grid_parameters(grid_file)
    ab = fits_to_array(filename)[0]
    ab = np.transpose(ab)
    if xmin is None:
        xmin = 1
    if xmax is None:
        xmax = np.max(r)
    if ymin is None:
        ymin = 0
    if ymax is None:
        if plot_z == False:
            ymax = np.max(z/r)
        else:
            ymax = np.max(z)
    if plot_z == False:
        y_z = z/r
    else:
        y_z = z
    plt.pcolor(r, y_z, ab, vmin = colormin, vmax = colormax, norm = colors.LogNorm(), cmap = colormap)
    plt.colorbar()
    plt.title('Molecular abundance over the disk')
    if xscale != None:
        plt.xscale(xscale)
    plt.xlim((xmin,xmax))
    plt.ylim((ymin,ymax))
    plt.xlabel('R (au)')
    if plot_z == False:
        plt.ylabel('z/R')
    else:
        plt.ylabel('z')
    contours = np.arange(int(np.log10(colormin)),int(np.log10(colormax))+1)
    contours = np.power(float(10), contours)
    if draw_contours == True:
        cs = plt.contour(r, y_z, ab, contours, colors = 'black')
        cs.clabel(contours, inline = 1, fmt = '%.1e', colors = 'black', manual = manual_labels)
    plt.savefig(savename)
    plt.close()

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING WARNING WARNING Dot not use pl_fits to plot the temperature, you should really use this function instead, as the colorbar scale is more specific here
# Reads temperature fits file and plot it on a graph
def pl_temperature(filename = 'temperature.fits', savename = 'temperature.png', colormin = 0, colormax = 200, colormap = 'gist_rainbow_r', xscale = 'log', xmin = None, xmax = None, ymin = None, ymax = None, draw_contours = False, manual_labels = False, grid_file = 'grid.fits', plot_z = False, c_interv = 10, font = 15, savefig = True, show = False, close = True):
    mpl.rcParams['font.size'] = font
    r,z = grid_parameters(grid_file)
    temp = fits_to_array(filename)[0]
    temp = np.transpose(temp)
    if xmin is None:
        xmin = 1
    if xmax is None:
        xmax = np.max(r)
    if ymin is None:
        ymin = 0
    if ymax is None:
        if plot_z == False:
            ymax = np.max(z/r)
        else:
            ymax = np.max(z)
    if plot_z == False:
        y_z = z/r
    else:
        y_z = z
    plt.pcolor(r, y_z, temp, vmin = colormin, vmax = colormax, cmap = colormap)
    plt.colorbar()
    if xscale != None:
        plt.xscale(xscale)
    plt.xlim((xmin,xmax))
    plt.ylim((ymin,ymax))
    plt.xlabel('R (au)')
    if plot_z == False:
        plt.ylabel('z/R')
    else:
        plt.ylabel('z')
    contours = np.arange(10,colormax+1, c_interv)
    if draw_contours == True:
        cs = plt.contour(r, y_z, temp, contours, colors = 'black')
        cs.clabel(contours, inline = 1, fmt = '%d', colors = 'black', manual = manual_labels)
    plt.title('Temperature over the disk')
    if show == True:
        plt.show()
    if savefig == True:
        plt.savefig(savename, bbox_inches = 'tight')
    if close == True:
        plt.close()

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING Obsolete, use pl_fits instead
# Reads gas density, and CN abundance, then draws CN density
def pl_CN_density(savename = 'CN_density.png', ab_filename = 'abundance.fits', dens_filename = 'gas_density.fits', grid_filename = 'grid.fits', xmin = None, xmax = None, ymin = None, ymax = None, xscale = 'log', colormap = 'gist_rainbow_r', plot_z = False, draw_contours = False, manual_labels = False, colormin = None, colormax = None):
    r,z = grid_parameters(grid_filename)
    ab = fits_to_array(ab_filename)[0]
    ab = np.transpose(ab)
    dens = fits_to_array(dens_filename)[0]
    dens = np.transpose(dens)
    uma = uma_cgs
    au = au_cgs
    mu = 2.8
    dens = dens/(mu*uma)
    n_CN = np.multiply(ab,dens)
    array_to_fits('CN_density.fits', np.transpose(n_CN))
    if xmin is None:
        xmin = 1
    if xmax is None:
        xmax = np.max(r)
    if ymin is None:
        ymin = 0
    if ymax is None:
        if plot_z == False:
            ymax = np.max(z/r)
        else:
            ymax = np.max(z)
    if plot_z == False:
        y_z = z/r
    else:
        y_z = z
    
    if colormax is None:
        colormax = np.max(n_CN)
    if colormin is None:
        colormin = colormax / (1e9)
    plt.pcolor(r, y_z, n_CN, vmin = colormin, vmax = colormax, norm = colors.LogNorm(), cmap = colormap)
    plt.colorbar()
    if xscale != None:
        plt.xscale(xscale)
    plt.xlim((xmin,xmax))
    plt.ylim((ymin,ymax))
    plt.xlabel('R (au)')
    if plot_z == False:
        plt.ylabel('z/R')
    else:
        plt.ylabel('z')
    contours = np.arange(int(np.log10(colormin)),int(np.log10(colormax))+1)
    contours = np.power(float(10), contours)
    if draw_contours == True:
        cs = plt.contour(r, y_z, n_CN, contours, colors = 'black')
        cs.clabel(contours, inline = 1, fmt = '%.1e', colors = 'black', manual = manual_labels)
    #plt.show()
    plt.title('CN density over the disk')
    plt.savefig(savename)
    plt.close()


#_________________________________________________________________________________
#_________________________________________________________________________________
# Create the abundance.fits file from Cazzoletti grid
def abundance_cazz(directory = './',
                   savename = 'abundance.fits',
                   resize = None,
                   sigma_resize = 1,
                   zexpand = 1,
                   abmax_factor = 1,
                   cazz_extend_dir = cazzoletti_directory,
                   cazz_extend_up = 'cazzoletti_extended_up.txt',
                   cazz_extend_low = 'cazzoletti_extended_low.txt',
                   cazz_extend_abun = 'cazzoletti_extended_abun.txt'):
    
    # Cazzoletti parameters, and converting z/R into z
    r_cazznorm, ab_cazznorm         = np.loadtxt(cazz_extend_dir + cazz_extend_abun, delimiter = ";", unpack=True, usecols=[0,1])
    r_extended_up, z_extended_up    = np.loadtxt(cazz_extend_dir + cazz_extend_up, delimiter = ";", unpack=True, usecols=[0,1])
    r_extended_low, z_extended_low  = np.loadtxt(cazz_extend_dir + cazz_extend_low, delimiter = ";", unpack=True, usecols=[0,1])
    #z_extended_up = z_extended_up * r_extended_up
    #z_extended_low = z_extended_low * r_extended_low
    
    # Cazzoletti peak abundance value and contour value
    cazz_abmax = 1.4e-7
    abcontour = 1e-8
    
    # Reads grid structure
    grid_filename = directory + 'data_disk/grid.fits'
    [r_grid,z_grid] = grid_parameters(grid_filename)
    r_grid_max = len(r_grid)
    z_grid_max = len(r_grid[0])
    
    # Compute grid limits and inner cell subdivision
    rf = r_grid[r_grid_max -1][0]
    rff = r_grid[r_grid_max -2][0]
    q = rf/rff
    r0 = r_grid[0][0]
    r1 = r0*q
    n_int = 0
    while r_grid[n_int][0] < r1:
        n_int += 1
    print('n_int = ' +str(n_int))
    r_out = int(r0 * q**(r_grid_max - n_int +1))
    print('r_out = ' + str(r_out))
    
    # Compute resizing factor if not provided
    if resize is None:
        resize = r_out / np.max(r_cazznorm)
    
    # Apply resizing parameters
    r_cazznorm      = np.dot(r_cazznorm, resize)
    r_extended_up   = np.dot(r_extended_up, resize)
    r_extended_low  = np.dot(r_extended_low, resize)
    z_extended_up   = np.dot(z_extended_up, resize * zexpand)
    z_extended_low  = np.dot(z_extended_low, resize * zexpand)
    cazz_abmax      = cazz_abmax * abmax_factor
    abcontour       = abcontour * abmax_factor
    
    # Interpolations
    f_cazznorm    = interp1d(r_cazznorm, ab_cazznorm, kind = 'linear')
    f_contour_up  = interp1d(r_extended_up, z_extended_up, kind = 'linear')
    f_contour_low = interp1d(r_extended_low, z_extended_low, kind = 'linear')
    
    # Abundance grid
    abundance = np.zeros((r_grid_max, z_grid_max))
    
    # Interpolations limits
    rmin = np.max([np.min(r_cazznorm), np.min(r_extended_low), np.min(r_extended_up)])
    rmax = np.min([np.max(r_cazznorm), np.max(r_extended_low), np.max(r_extended_up)])
    print('rmin = ' +str(rmin))
    print('rmax = ' +str(rmax))
    
    # Building abundance map
    for i in range(r_grid_max):
        radius = r_grid[i][0]
        print('cell : ' + str(i) + '/' +str(r_grid_max-1))
        if radius > rmin and radius < rmax:
            zu = f_contour_up(radius)
            zl = f_contour_low(radius)
            abmax = f_cazznorm(radius) * cazz_abmax
            dz = zu - zl
            zmax = (zl + zu)/2
            adjust = (1.+0.15/(1.+(radius/100)**2.2))
            sigma = sigma_resize * adjust * dz/np.sqrt(8*np.log(abmax/abcontour))
            abundance[i] = abmax*np.exp(-(z_grid[i]-zmax)**2/2./sigma**2)
    
    # Saving abundance map
    abundance = np.transpose(abundance)
    array_to_fits(directory + savename, abundance)


#_________________________________________________________________________________
#_________________________________________________________________________________
# Creates the abundance.fits file by placing CN at given abundance if temperature is between given values
def abundance_temp(directory = './',
                   savename = 'abundance.fits',
                   temp_min = 15,
                   temp_max = 25,
                   ab_value = 1e-8):
    temp_filename = directory + 'data_th/Temperature.fits'
    temp = fits_to_array(temp_filename)[0]
    ab = np.zeros(np.shape(temp))
    for i in range(len(temp)):
        for j in range(len(temp[0])):
            t = temp[i][j]
            if t < temp_max and t > temp_min:
                ab[i][j] = ab_value
    array_to_fits(savename, ab)

#_________________________________________________________________________________
#_________________________________________________________________________________
# Creates the abundance.fits file by placing CN at given abundance if temperature is between given values
def abundance_temp_bis(directory = './',
                   savename = 'abundance.fits',
                   temp_min = 15,
                   temp_max = 25,
                   temp_mid = 20,
                   distance = 12,
                   height = 100,
                   ab_value = 1e-8):
    temp_filename = directory + 'data_th/Temperature.fits'
    grid_filename = directory + 'data_disk/grid.fits'
    temp = fits_to_array(temp_filename)[0]
    temp = np.transpose(temp)
    [r,z] = grid_parameters(grid_filename)
    ab = np.zeros(np.shape(temp))
    
    contours = [temp_mid]

    cs = plt.contour(r,z,temp,contours)

    data = cs.allsegs

    #coords = []
    #for k in range(len(contours)):
        #x = [data[0][i][0] for i in range(len(data[0]))]
        #y = [data[0][i][1] for i in range(len(data[0]))]
        #val = [x,y]
        #coords.append(val)
    
    #r_list = [r[i][0] for i in range(len(r))]
    #interp = interp1d(coords[0][0],coords[0][1], bounds_error = False, fill_value = 0)
    #y = [interp(r_list[i]) for i in range(len(r_list))]

    #for i in range(len(coords[0][0])):
        #print(coords[0][0][i], coords[0][1][i])

    #plt.plot(r_list,y)
    #plt.legend()
    #plt.show()
    for i in range(len(r)):
        for j in range(len(r[0])):
            t_val = temp[i][j]
            if t_val <= temp_max and t_val >= temp_min and z[i][j] <= height:
                ab[i][j] = ab_value
            #d = abs(z[i][j] - interp(r[i][j]))
            #if d <= distance:
                #ab[i][j] = ab_value
    
    array_to_fits('abundance.fits',np.transpose(ab))




#_________________________________________________________________________________
#_________________________________________________________________________________
# Makes the channel map
def channel_map(filename = 'lines.fits',
                savename = 'channelmap.png',
                vmin = -5,
                vmax = 5,
                interv = 0.2,
                colormin = None,
                colormax = None,
                colormap = 'sls',
                sls_file = sls_file,
                textcolor = 'black',
                xmin_g = None,
                xmax_g = None,
                ymin_g = None,
                ymax_g = None,
                font = None,
                v_lsr = 0,
                savefig = True,
                show = False,
                close = True):
    R,V,B = np.loadtxt(sls_file, delimiter = " ", unpack=True, usecols=[0,1,2])
    a = len(R)
    my_map = []
    for i in range(a):
        my_map.append((R[i],V[i],B[i]))
    cm = colors.LinearSegmentedColormap.from_list('sls',my_map)
    if colormap == 'sls':
        colormap = cm
    # Read fits file parameters
    ray = fits.open(filename)
    x_pixel_size = ray[0].header['CDELT1']    # Pixel size in deg, along x axis
    y_pixel_size = ray[0].header['CDELT2']    # Pixel size in deg, along y axis
    xlength = ray[0].header['NAXIS1']         # Number of pixels along x axis
    ylength = ray[0].header['NAXIS2']         # Number of pixels, along y axis
    slices = ray[0].header['NAXIS3']          # Number of v slices
    v_interv = ray[0].header['CDELT3']     # Speed interval between slices

    # Compute dimensions of map
    xsize = abs(x_pixel_size * 3600)            # Convert into arcsec
    ysize = (y_pixel_size * 3600)               # Convert into arcsec
    xmin = -int(xlength/2)
    xmax = int(xlength/2)
    ymin = -int(ylength/2)
    ymax = int(ylength/2)
    
    # Grid in arcsec
    #x = []
    #y = []
    #for i in range(xmin, xmax +1):
        #x.append(i*xsize)
    
    #for i in range(ymin, ymax +1):
        #y.append(i*ysize)
    [x,y] = coordinates(filename)
    
    ray = fits_to_array(filename)[0]
    
    # Entire list of velocities
    slice_min = -int(slices/2)
    slice_max = int(slices/2)
    v_list_tot = [float('%.3f'%(i * v_interv + v_lsr)) for i in range(slice_min, slice_max +1)]
    v_list_tot = make_coord_list(filename = filename, axis = 3)
    v_list_tot = np.add(v_list_tot, v_lsr)
    
    # Check if entry parameters are correct
    if (vmin not in v_list_tot) or (vmax not in v_list_tot):
        vmin = v_list_tot[find_nearest(v_list_tot, vmin)]
        vmax = v_list_tot[find_nearest(v_list_tot, vmax)]
        print('vmin or vmax not in vlist, vmin = {:.3f} and vmax = {:.3f} will be used'.format(vmin,vmax))
    rat = (interv*100/v_interv)/100
    print('# Ratio = ', rat)
    #if int(rat) != rat:
        #print('# interv must be a multiple of ', v_interv)
        #exit(0)
    # Plotted list of velocities
    if vmin == vmax:
        v_list = [vmin]
    else:
        v_list = np.round(np.arange(vmin,vmax,interv),3)
    if v_list[-1] != vmax:
        v_list = np.append(v_list,vmax)
    
    slice_list = []
    #for i in range(slices):
        #if v_list_tot[i] in v_list:
            #slice_list.append(i)
    for i in v_list:
        slice_list.append(find_nearest(v_list_tot,i))
    
    numb_sl = len(slice_list)
    
    print('# Slices list :', slice_list)
    print('# Speed list :', v_list)
    # Dimensions of the channel map
    # Number of rows and columns. Making a square if possible, else, n_rows = n_columns +1
    numb_sl = len(slice_list)
    n_columns = np.sqrt(numb_sl)
    if n_columns == int(n_columns):
        n_columns = int(n_columns)
        n_rows = n_columns
    else:
        n_rows = int(n_columns) +1
        n_columns = int(np.round(n_columns))
    
    # Color limits
    if colormax is None:
        colormax = np.max(ray)
    if colormin is None:
        colormin = colormax / 1e3
    
    # Plotting channelmap
    
    if np.sign(colormin) == np.sign(colormax):
        norm = colors.LogNorm()
    else:
        norm = colors.Normalize()
    print('# numb_sl', numb_sl)
    
    print('# ncolumns', n_columns)
    fig_size = 30
    if font == None:
        font = 20
    speed_font = 0.8*int(font * fig_size/20 * 6/n_columns)
    axes_font = 0.8*speed_font
    mpl.rcParams['font.size'] = axes_font
    fig, axes = plt.subplots(n_rows,n_columns, sharex = True, sharey = True, figsize = (fig_size,fig_size))
    for i in range(numb_sl):
        print(i)
        xplot = i//n_columns
        yplot = i%n_columns
        print(xplot, yplot)
        if n_rows == 1 and n_columns ==1:
            ax = axes
        elif n_rows == 1 or n_columns == 1:
            ax = axes[yplot]
        else:
            ax = axes[xplot, yplot]
        ar = ray[slice_list[i]]
        im = ax.pcolor(x,y,ar, norm =  norm, vmin = colormin, vmax = colormax, cmap = colormap)
        ax.yaxis.set_minor_locator(MultipleLocator(1))
        ax.xaxis.set_minor_locator(MultipleLocator(1))
        ax.yaxis.set_major_locator(MultipleLocator(2))
        ax.xaxis.set_major_locator(MultipleLocator(2))
        ax.set_xlim(xmin_g , xmax_g)
        ax.set_ylim(ymin_g,ymax_g)
        #ax.set_xlim(ax.get_xlim()[::-1])
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()
        x_space = int(xlim[1]-xlim[0])//2
        if x_space == 0:
            x_space = 0.5
        xticks = np.arange(int(xlim[0]), xlim[1], x_space)
        y_space = int(ylim[1]-ylim[0])//2
        if y_space == 0:
            y_space = 0.5
        yticks = np.arange(int(ylim[0]), ylim[1], y_space)
        ax.set_xticks(xticks)
        ax.set_yticks(yticks)
        fig.subplots_adjust(hspace = 0, wspace = 0)
        #ax.set_xlim(ax.get_xlim()[::-1])
        if xmin_g is None:
            x_text = ax.get_xlim()[1] -0.5
        else:
            x_text = xmax_g - 0.5
        if ymax_g is None:
            y_text = ax.get_ylim()[1] -0.5
        else:
            y_text = ymax_g -0.5
        ax.text(x_text,y_text, 'v = {:.3f} km.s-1'.format(v_list[i]), color = textcolor, fontsize = speed_font)
        ax.grid()
    ax.set_xlim(ax.get_xlim()[::-1])
    cbar_ax = fig.add_axes([0.92, 0.15, 0.025, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    fig.text(0.5,0.04, 'x (arcsec)', ha = 'center')
    fig.text(0.04,0.5, 'y (arcsec)', va = 'center', rotation = 'vertical')
    if show == True:
        plt.show()
    if savefig == True:
        plt.savefig(savename, bbox_inches = 'tight')
    if close == True:
        plt.close()


#_________________________________________________________________________________
#_________________________________________________________________________________
# Returns the closest index to value in given array
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx




#_________________________________________________________________________________
#_________________________________________________________________________________
# Plot the intensity of the integrated_over_channels channel map along a given axis : either x or y. Default is a slice at y = 0". Slice coordinate should be given in arcsec.
def pl_intensity(axis = 'y', slice_at = 0, filename = 'convolve.fits', normalize = False, savefig = True, show = False, close = True):
    
    lines = fits.open(filename)
    hdr = lines[0].header

    ar = fits_to_array(filename)[0]
    
    if axis == 'y':
        tar = []
        for i in ar:
            tar.append(np.transpose(i))
        ar = tar
        ax_val = 1
    elif axis != 'x':
        print('Axis must be either x or y')
        exit(0)
    else:
        ax_val = 0
    
    pos = coordinates(filename = filename, make_cube = False)
    
    pos_list = pos[ax_val]
    if slice_at not in pos_list:
        slice_index = find_nearest(pos_list,slice_at)
        slice_at = pos_list[slice_index]
        print('This coordinate is not in the grid')
        print('Closest coordinate is : ', slice_at)
    else:
        slice_index = np.where(pos_list == slice_at)

    tot_emission = np.zeros(np.shape(ar[0]))
    for i in range(len(ar)):
        tot_emission = np.add(tot_emission, ar[i])
    
    vals = []
    other_list = pos[abs(ax_val-1)]
    for i in range(len(other_list)):
        vals.append(tot_emission[i][slice_index])
    
    if normalize == True:
        vals = np.divide(vals, np.max(vals))
    
    plt.plot(other_list, vals)
    if axis == 'x':
        x_axis = 'y'
    else:
        x_axis = 'x'
    plt.xlabel(x_axis + ' (arcsec)')
    plt.ylabel('Integrated intensity')
    savename = 'intensity_axis_' + axis + '_at_{:.3f}.png'.format(slice_at)
    if show == True:
        plt.show()
    if savefig == True:
        plt.savefig(savename)
    if close == True:
        plt.close()


#_________________________________________________________________________________
#_________________________________________________________________________________
# Plots the spectrum at the given coordinate
def pl_spectrum(filename,
                loc = [0,0],
                vmin = None,
                vmax = None,
                normalize = False,
                v_lsr = 3.755,
                remove_lsr = False,
                savefig = True,
                show = False,
                close = True,
                savename = 'spectrum.png'):
    array = fits_to_array(filename)[0]
    
    vlist = make_coord_list(filename = filename, axis = 3)
    if remove_lsr == True:
        vlist = np.subtract(vlist,v_lsr)
    
    pos = coordinates(filename = filename, make_cube = False)

    xindex = find_nearest(pos[0],loc[0])
    yindex = find_nearest(pos[1],loc[1])
    
    val = []
    for i in range(len(array)):
        val.append(array[i][yindex][xindex])
    
    maxi = np.max(val)
    print(maxi)
    if normalize == True:
        if maxi == 0:
            return 0
        val = np.divide(val, maxi)
    plt.plot(vlist,val, label = str(filename) + ' x = {:.3f}", y = {:.3f}"'.format(pos[0][xindex], pos[1][yindex]))
    plt.xlabel('Speed (km/s)')
    plt.legend()
    plt.xlim((vmin,vmax))
    if show == True:
        plt.show()
    if savefig == True:
        plt.savefig(savename, bbox_inches = 'tight')
    if close == True:
        plt.close()
    return 1


#_________________________________________________________________________________
#_________________________________________________________________________________
# Turns Frequency header to speed header
def hdr_freq_to_speed(filename, nu0 = 226.87478e9):
    c = c_si / 1000
    lines = fits.open(filename, 'update')
    hdr = lines[0].header
    if hdr['CTYPE3'] != 'VELO-LSR':
        hdr['CTYPE3'] = 'VELO-LSR'
        hdr['CRVAL3'] = -c * (hdr['CRVAL3']-nu0)/nu0
        hdr['CDELT3'] = -c * hdr['CDELT3']/nu0
    lines.close()


def draw_dutrey_contours(marker = '+', savename = 'dutrey_h2_density_contours.png', savefig = False, close = False, legend = False, show = False, directory = '/user/homedir/demarsd/Documents/stage/data/dutrey/'):
    names = ['h2_density_1e5.txt','h2_density_1e6.txt','h2_density_1e7.txt','h2_density_1e8.txt']
    for name in names:
        [r,z] = np.loadtxt(directory + name, delimiter = ";", unpack=True, usecols=[0,1])
        plt.scatter(r,z, marker = marker, label = name)
    if legend == True:
        plt.legend()
    if show == True:
        plt.show()
    if savefig == True:
        plt.savefig(savename, bbox_inches = 'tight')
    if close == True:
        plt.close()
